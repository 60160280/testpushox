
public class Game {
	private Board board;
	private Player x;
	private Player o;

	public Game() {
		o = new Player('O');
		x = new Player('X');
		board = new Board(x, o);
	}

	public void play() {
		showwelcome();
		showTable();
		showTurn();
	}

	private void showwelcome() {
		System.out.println("Welcome to OX Game");

	}

	private void showTable() {
		char[][] table = board.getTable();
		System.out.println("  1 2 3");
		for (int i = 0; i < table.length; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(" " + table[i][j]);
			}
			System.out.println();
		}
	}

	private void showTurn() {
		System.out.print(board.getCurrent().getName()+ " Turn");
	}

}
