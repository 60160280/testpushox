
public class Board {
	char [][] table = {
			{'-','-','-'},
			{'-','-','-'},
			{'-','-','-'},
	};
	private Player x; 
	private Player o;
	private Player winner;
	private Player current;
	private int turnCount;
	
	public Board(Player x , Player o) {
		this.x = x;
		this.o = o;
		current = x;
		winner = null;
		turnCount = 0 ;
	}
	
	public char[][] getTable() {
		return table;
	}
	public Player getCurrent() {
		return current;
	}
}
